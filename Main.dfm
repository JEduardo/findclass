object Principal: TPrincipal
  Left = 495
  Top = 109
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Identificador'
  ClientHeight = 454
  ClientWidth = 654
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 8
    Width = 200
    Height = 13
    Alignment = taCenter
    AutoSize = False
    Caption = 'Handle'
  end
  object Label2: TLabel
    Left = 16
    Top = 48
    Width = 200
    Height = 13
    Alignment = taCenter
    AutoSize = False
    Caption = 'Classe'
  end
  object Label3: TLabel
    Left = 16
    Top = 88
    Width = 200
    Height = 13
    Alignment = taCenter
    AutoSize = False
    Caption = 'Titulo'
  end
  object edtClasse: TEdit
    Left = 16
    Top = 64
    Width = 200
    Height = 21
    TabOrder = 0
  end
  object edthandle: TEdit
    Left = 16
    Top = 24
    Width = 200
    Height = 21
    TabOrder = 1
  end
  object edttexto: TEdit
    Left = 16
    Top = 104
    Width = 200
    Height = 21
    TabOrder = 2
  end
  object Button1: TButton
    Left = 72
    Top = 144
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 3
    OnClick = Button1Click
  end
  object ListBox1: TListBox
    Left = 8
    Top = 176
    Width = 625
    Height = 257
    ItemHeight = 13
    TabOrder = 4
  end
  object edtFineste: TEdit
    Left = 272
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 5
  end
  object Button2: TButton
    Left = 288
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Finalizar Este'
    TabOrder = 6
    OnClick = Button2Click
  end
  object Timer: TTimer
    OnTimer = TimerTimer
    Left = 176
    Top = 88
  end
  object Timer1: TTimer
    Interval = 10000
    Left = 24
    Top = 104
  end
end
